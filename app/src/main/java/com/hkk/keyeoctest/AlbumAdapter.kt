package com.hkk.keyeoctest

import android.graphics.Color
import android.net.Uri
import android.widget.ImageView
import androidx.core.content.ContextCompat
import com.chad.library.adapter.base.BaseQuickAdapter
import com.chad.library.adapter.base.viewholder.BaseViewHolder
import com.google.gson.JsonElement
import com.squareup.picasso.Picasso
import java.io.File
import java.text.SimpleDateFormat
import java.util.*


class AlbumAdapter(val list: MutableList<JsonElement>) :
    BaseQuickAdapter<JsonElement, BaseViewHolder>(
        R.layout.row_album, list
    ) {


    var bookmarkList = mutableListOf<JsonElement>()
    var isBookmark = false

    override fun convert(holder: BaseViewHolder, item: JsonElement) {
        val jsonObject = item.asJsonObject

        val imageView = holder.getView<ImageView>(R.id.imageView)
        Picasso.get().load(jsonObject.get("artworkUrl100").asString).into(imageView);

        holder.setText(R.id.tv1, jsonObject.get("artistName").asString)
        holder.setText(
            R.id.tv2,
            jsonObject.get("collectionName").asString
        )

        val bookmarkButton = holder.getView<ImageView>(R.id.bookmark)

        if (bookmarkList.contains(item)) {
            bookmarkButton.setImageResource(R.drawable.ic_baseline_bookmark_added_24)
        } else {
            bookmarkButton.setImageResource(R.drawable.ic_baseline_bookmark_add_24)
        }

        bookmarkButton.setOnClickListener {
            if (isBookmark) {
                bookmarkList.remove(item)
                notifyItemRemoved(holder.layoutPosition)
            } else {
                if (bookmarkList.contains(item)) {
                    bookmarkList.remove(item)
                } else {
                    bookmarkList.add(item)
                }
                notifyItemChanged(holder.layoutPosition)
            }
        }

        holder.setBackgroundColor(
            R.id.container,
            if (holder.layoutPosition % 2 == 0) Color.WHITE else ContextCompat.getColor(
                context,
                R.color.light_gray
            )
        )
    }

    fun toggleBookMark(isBookmark: Boolean) {
        this.isBookmark = isBookmark
        setNewInstance(if (isBookmark) bookmarkList else list)
    }

}