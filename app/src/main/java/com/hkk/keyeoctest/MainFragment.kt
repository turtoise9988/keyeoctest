package com.hkk.keyeoctest

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.GridLayoutManager
import com.github.kittinunf.fuel.Fuel
import com.google.gson.JsonParser
import com.hkk.keyeoctest.databinding.FragmentMainBinding
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch


class MainFragment : Fragment() {

    private lateinit var binding: FragmentMainBinding

    var isBookmark = false

    private lateinit var albumAdapter: AlbumAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(false)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentMainBinding.inflate(inflater, container, false)
        return binding.root
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.recyclerView.layoutManager = GridLayoutManager(requireContext(), 2)

        binding.bookmark.setOnClickListener {
            isBookmark = !isBookmark
            binding.bookmark.text = if (isBookmark) "Show All" else "Show Bookmarks"
            albumAdapter.toggleBookMark(isBookmark)
        }

        GlobalScope.launch {

            Fuel.get("https://itunes.apple.com/search?term=jack+johnson&entity=album")
                .response { request, response, result ->
                    val (bytes, error) = result
                    if (bytes != null) {
                        val jsonResult = JsonParser.parseString(String(bytes)).asJsonObject
                        val albumArray = jsonResult.get("results").asJsonArray
                        albumAdapter = AlbumAdapter(albumArray.asList())
                        binding.recyclerView.adapter = albumAdapter
                    }
                }
        }


    }

}